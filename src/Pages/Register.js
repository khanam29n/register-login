import React, { Component } from "react";
import axios from "axios";

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      mobile: "",
      password: "",
      message: "",
    };
  }

  handleInputChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append("name", this.state.name);
    formData.append("email", this.state.email);
    formData.append("mobile", this.state.mobile);
    formData.append("password", this.state.password);

    axios
      .post("https://ohkolkata.com/json_api/demo_register.php", formData, {})
      .then((response) => {
        this.setState({
          message: "Successfully Registered",
        });
        console.log(response);
      })
      .catch((err) => {
        this.setState({
          message: "Email ID or Mobile Number Already Registered",
        });
        console.log(err.message);
      });
  };

  render() {
    return (
      <div className="container">
        <h2>Register</h2>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>Name</label>
            <input
              type="text"
              className="form-control"
              placeholder="Enter name"
              name="name"
              value={this.state.name}
              onChange={this.handleInputChange}
              required
            />
          </div>
          <div className="form-group">
            <label>Email address</label>
            <input
              type="email"
              className="form-control"
              placeholder="Enter email"
              name="email"
              value={this.state.email}
              onChange={this.handleInputChange}
              required
            />
          </div>
          <div className="form-group">
            <label>Mobile</label>
            <input
              type="number"
              className="form-control"
              placeholder="Enter phone number"
              name="mobile"
              value={this.state.mobile}
              onChange={this.handleInputChange}
              required
            />
          </div>
          <div className="form-group">
            <label>Password</label>
            <input
              type="password"
              className="form-control"
              placeholder="Enter password"
              name="password"
              value={this.state.password}
              onChange={this.handleInputChange}
              required
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Register
          </button>
          {this.state.message && (
            <div className="mt-3 alert alert-info">{this.state.message}</div>
          )}
        </form>
      </div>
    );
  }
}

export default Register;
