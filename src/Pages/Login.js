import React, { Component } from "react";
import axios from "axios";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user_name: "",
      password: "",
      message: "",
    };
  }

  handleInputChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleLogin = (e) => {
    console.log("login here", this.state);
    e.preventDefault();

    const formData = new FormData();
    formData.append("user_name", this.state.user_name);
    formData.append("password", this.state.password);

    axios
      .post("https://ohkolkata.com/json_api/demo_login.php", formData)
      .then((response) => {
        this.setState({
          message: "Welcome to ZWV",
        });
        console.log(response);
      })
      .catch((err) => {
        this.setState({
          message: " Incorrect username or password",
        });
        console.log(err.message);
      });
  };

  render() {
    return (
      <div className="container">
        <h2>Login</h2>

        <form onSubmit={this.handleLogin}>
          <div className="form-group">
            <label>Username</label>
            <input
              className="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              placeholder="Enter user_name"
              type="text"
              name="user_name"
              value={this.state.user_name}
              onChange={this.handleInputChange}
              required
            />
          </div>
          <div className="form-group">
            <label>Password</label>
            <input
              type="password"
              className="form-control"
              id="exampleInputPassword1"
              placeholder="Password"
              name="password"
              value={this.state.password}
              onChange={this.handleInputChange}
              required
            />
          </div>

          <button type="submit" className="btn btn-primary">
            Login
          </button>

          <small id="emailHelp" className="form-text text-muted">
            {this.state.message}
          </small>
        </form>
      </div>
    );
  }
}

export default Login;
